//
//  ViewController.swift
//  MBDoubleSliderExample
//
//  Created by Viorel Porumbescu on 19/01/2017.
//  Copyright © 2017 Viorel. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBAction func didChange(_ sender: MBDoubleSlider) {
        Swift.print("slider === \(sender.firstValue, sender.secondValue)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

