Pod::Spec.new do |s|

  s.name         = 'MBDoubleSlider'
  s.version      = '0.1.0'
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { 'Cornel Viorel' => 'https://github.com/c-Viorel' }
  s.homepage     = 'http://degradr.photo'
  s.summary      = 'CustomDoubleSlider for macOs written in swift 3.0'
  s.description  = 'CustomDoubleSlider for macOs written in swift 3.0'

  s.source       = { :git => 'https://bitbucket.org/degrader/mbdoubleslider', :tag => s.version}

  s.osx.deployment_target = "10.12"
  
  s.source_files        = 'MBDoubleSlider/*.{h,swift,m}'
  s.public_header_files = 'MBDoubleSlider/*.h'
  s.header_dir   = 'MBDoubleSlider'

s.requires_arc = true

end
