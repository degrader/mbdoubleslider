//
//  MBDoubleSlider.swift
//  customDoubleSlider
//
//  Created by Viorel Porumbescu on 18/10/15.
//  Copyright (c) 2015 Viorel Porumbescu. All rights reserved.
//

import Cocoa

public protocol MBDoubleSliderDelegate {
    func controller(_ controller: MBDoubleSlider , didChangeFirstValue: CGFloat , secondValue: CGFloat)
    // TODO: shold be an optional protocol.
    // func controller(_ controller: MBDoubleSlider, willBeginChangeValues first:CGFloat, second:CGFloat)
}

@IBDesignable
public class MBDoubleSlider: NSControl {
    
    @IBInspectable public var backgroundLineColor: NSColor = NSColor.white {didSet { setNeedsDisplay()}}
    @IBInspectable public var selectionLineColor: NSColor  = NSColor.gray {didSet { setNeedsDisplay()}}
    @IBInspectable public var textColor:NSColor            = NSColor.gray {
        didSet {
            firstLabel.textColor = textColor
            secondLabel.textColor = textColor
            setNeedsDisplay()
        }
    }
    @IBInspectable public var textFont:NSFont?             = NSFont(name: "HelveticaNeue", size: 22) {
        didSet {
            firstLabel.font  = textFont
            secondLabel.font  = textFont
            setNeedsDisplay()
        }
    }
    
    public var trackLabelformat:String = "%.0f" { didSet{ setNeedsDisplay() } }
    
    // TODO: Add option to hide info labels
    
    public var minValue: CGFloat                           = 0 
    public var maxValue: CGFloat                           = 100
    
    public var firstValue:   CGFloat                       = 0      { 
        didSet{ if
            firstValue  < minValue   || firstValue  > secondValue { firstValue  = minValue };  
            setNeedsDisplay()
        }
    }
    public var secondValue:  CGFloat                       = 100    { 
        didSet{ 
            if secondValue < firstValue || secondValue > maxValue    { secondValue = maxValue } ; 
            setNeedsDisplay()
        }
    }
    
    public var minimValue:CGFloat                          = 10
    
    public var delegate: MBDoubleSliderDelegate?
    
    // Custom Private Vars
    fileprivate var firstKnob:MBCustomKnob      = MBCustomKnob()
    fileprivate var secondKnob:MBCustomKnob     = MBCustomKnob()
    fileprivate lazy var firstLabel:NSTextField =  {
        return NSTextField()
    }() 
    fileprivate lazy var secondLabel:NSTextField      =  {
        return NSTextField()
    }() 

    fileprivate var yOrigin:CGFloat             = 0
    fileprivate var lineMaxWidh: CGFloat        = 0
    fileprivate var shouldMoveFirst:Bool        = false
    fileprivate var shouldMoveLast: Bool        = false
        
    override public init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        setUpLabels()
    }
        
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpLabels()
    }
    
    private func lableSize(_ value:CGFloat) -> NSSize {
        return self.valueToString(value).size(withAttributes: [NSFontAttributeName : self.textFont ?? NSFont.systemFont(ofSize: 12)])
    }
    
    override public func draw(_ dirtyRect: NSRect) {
        //super.draw(dirtyRect)
        reDraw()
    }
    
    var trackingArea:NSTrackingArea!
    override public func updateTrackingAreas() {
        if trackingArea != nil {
            self.removeTrackingArea(trackingArea)
        }
        trackingArea               = NSTrackingArea(rect: self.bounds, options: [NSTrackingAreaOptions.enabledDuringMouseDrag , NSTrackingAreaOptions.mouseMoved, NSTrackingAreaOptions.activeAlways, .cursorUpdate], owner: self, userInfo: nil)
        self.addTrackingArea(trackingArea)
    }
    
    
    /// Mouse down event : We test if current mouse location is inside of first or second knob. If yes, then we 
    // tell that selected knob that it can be moved
    override public func mouseDown(with theEvent: NSEvent) {
        
        let loc = self.convert(theEvent.locationInWindow, from: self.window?.contentView)
        let firstFrame = NSInsetRect(firstKnob.frame, -5, -5)
        let secFrame = NSInsetRect(secondKnob.frame, -5, -5)
        
        if NSPointInRect(loc, firstFrame) {
            shouldMoveFirst = true
        }
        if NSPointInRect(loc, secFrame) {
            shouldMoveLast = true
        }
    }
    
    // Mouse dragged Event : if is any selected knob we will move to new position, and we calculate
    // new the new slider values
    override public func mouseDragged(with theEvent: NSEvent) {
        var loc = self.convert(theEvent.locationInWindow, from: self.window?.contentView)
        
        let _ = self.convert(theEvent.locationInWindow, to: self)
        
        if shouldMoveFirst {
            
            loc.x -= firstKnob.frame.width/2
            
            let minim = CGFloat(0)
            let maxim   = secondKnob.frame.origin.x - firstKnob.frame.width - ((lineMaxWidh /  maxValue)  * minimValue )
            if loc.x > minim && loc.x < maxim {
                firstValue = (loc.x * maxValue) / lineMaxWidh
                if loc.x < minim {
                    firstValue = (minim * maxValue) / lineMaxWidh
                }
                if loc.x > maxim {
                    firstValue = (maxim * maxValue) / lineMaxWidh
                }
            }else if loc.x < minim {
                firstValue = (minim * maxValue) / lineMaxWidh
            }
            self.needsDisplay = true
            create()
        }
        if shouldMoveLast {
            
            loc.x -= secondKnob.frame.width/2
                        
            let minim = firstKnob.frame.origin.x + secondKnob.frame.width + ((lineMaxWidh /  maxValue)  * minimValue )
            let maxim   = lineMaxWidh
            if loc.x > minim && loc.x < maxim {
                secondValue = (loc.x * maxValue) / lineMaxWidh
                if loc.x < minim {
                    secondValue = (minim * maxValue) / lineMaxWidh
                }
                if loc.x > maxim {
                    secondValue = (maxim * maxValue) / lineMaxWidh
                }
            } else if loc.x > maxim  {
                secondValue = (maxim * maxValue) / lineMaxWidh
            }
            self.needsDisplay = true
            create()
        }
    }
    
    //Mouse up event : We "deselect" both knobs.
    override public func mouseUp(with theEvent: NSEvent) {
        shouldMoveLast  = false
        shouldMoveFirst = false
    }
    
    private func reDraw() {
        
        let textOrigin: CGFloat = 0
        let knobSize            = self.frame.height * 0.4
        lineMaxWidh             = self.frame.width - knobSize * 2
        yOrigin                 = self.frame.height * 0.6

        firstLabel.sizeToFit()
        secondLabel.sizeToFit()
        secondLabel.frame.size.width += 10
        
        let firstX   = (firstValue * lineMaxWidh) /  maxValue 
        var secondX  = (secondValue * lineMaxWidh) / maxValue
        
        firstKnob.setFrameSize(NSMakeSize(knobSize, knobSize))
        secondKnob.setFrameSize(NSMakeSize(knobSize, knobSize))
        firstKnob.setFrameOrigin(NSMakePoint(firstX, yOrigin))
        secondKnob.setFrameOrigin(NSMakePoint(secondX, yOrigin))
        
        firstLabel.setFrameOrigin(NSMakePoint(firstX, textOrigin))
        if secondX + secondLabel.frame.width > frame.width {
            secondX = frame.width - secondLabel.frame.width
        }
        secondLabel.setFrameOrigin(NSMakePoint(secondX, textOrigin))
                      
        // Center text Label if is posible
        if firstX > 8 {
            firstLabel.setFrameOrigin(NSMakePoint(firstX, textOrigin))
        }
        if secondX < lineMaxWidh - 8 {
            secondLabel.setFrameOrigin(NSMakePoint(secondX, textOrigin))
        }
        
        if secondX > lineMaxWidh - 8 {
            secondLabel.setFrameOrigin(NSMakePoint(lineMaxWidh - 16, textOrigin))
        }
        if (firstLabel.frame.origin.x + NSWidth(firstLabel.frame) ) > secondLabel.frame.origin.x {
            let size  = (secondLabel.frame.origin.x  - (firstLabel.frame.origin.x + NSWidth(firstLabel.frame) )) / 2
            var state = true
            if firstX < 8 {
                state = false
                secondLabel.setFrameOrigin(NSMakePoint(secondLabel.frame.origin.x - size - size, textOrigin))
            }
            if secondX > lineMaxWidh - 8 {
                state = false
                firstLabel.setFrameOrigin(NSMakePoint(firstLabel.frame.origin.x + size + size, textOrigin))
            }
            if state {
                firstLabel.setFrameOrigin(NSMakePoint(firstLabel.frame.origin.x + size, textOrigin))
                secondLabel.setFrameOrigin(NSMakePoint(secondLabel.frame.origin.x - size, textOrigin))
            }
        }
        
        firstLabel.stringValue   = valueToString(firstValue)
        secondLabel.stringValue  = valueToString(secondValue)
        
        // Draw  background line
        let backgroundLine              = NSBezierPath()
        backgroundLine.move(to: NSMakePoint(knobSize * 0.5,  self.frame.height * 0.8))
        backgroundLine.line(to: NSMakePoint(lineMaxWidh + knobSize * 0.5 ,  self.frame.height * 0.8))
        backgroundLine.lineCapStyle     = NSLineCapStyle.roundLineCapStyle
        backgroundLine.lineWidth        = 3
        backgroundLineColor.set()
        backgroundLine.stroke()
        ///Draw selection  line (the line between knobs)
        let selectionLine               = NSBezierPath()
        selectionLine.move(to: NSMakePoint(firstX + knobSize / 2 , self.frame.height * 0.8))
        selectionLine.line(to: NSMakePoint(secondX + knobSize / 2 , self.frame.height * 0.8))
        selectionLine.lineCapStyle      = NSLineCapStyle.roundLineCapStyle
        selectionLine.lineWidth         = 3
        selectionLineColor.setStroke()
        selectionLine.stroke()
        
        self.addSubview(firstKnob)
        self.addSubview(secondKnob)
        self.addSubview(firstLabel)
        self.addSubview(secondLabel)
    }
    
    
    private func setUpLabels(){
        firstLabel.isBordered       = false
        firstLabel.identifier       = "10"
        firstLabel.isEditable       = false
        firstLabel.isSelectable     = false
        firstLabel.stringValue      = valueToString(firstValue)
        firstLabel.backgroundColor  = NSColor.clear
        firstLabel.font             = textFont
        firstLabel.textColor        = textColor
        firstLabel.alignment        = NSTextAlignment.left
        
        secondLabel.isBordered      = false
        secondLabel.identifier      = "10"
        secondLabel.isEditable      = false
        secondLabel.isSelectable    = false
        secondLabel.stringValue     = valueToString(secondValue)
        secondLabel.backgroundColor = NSColor.clear
        secondLabel.font            = textFont
        secondLabel.textColor       = textColor
        secondLabel.alignment       = NSTextAlignment.left        
    }
    
    /// If has a delegate we will send  changed notification, and new values for slider.
    /// Also we trigger action if this control has one.
    private func create() {
        if self.action != nil {
            NSApp.sendAction(self.action!, to: self.target, from: self)
        }
        
        if let delegate = self.delegate {
            delegate.controller(self, didChangeFirstValue: firstValue, secondValue: secondValue)
        }
    }
    
    /// Convert seconds to 00:00 format
    fileprivate func valueToString(_ value:CGFloat) -> String{
        return String(format: trackLabelformat, value)
    }
    
}

